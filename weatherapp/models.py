from django.contrib.auth.models import User
from django.db.models.fields import IntegerField, CharField, DecimalField
from django.db.models import OneToOneField, ForeignKey, PROTECT, CASCADE
from django.db.models import Model
from django.db import models

class Location(Model):
    city_id = IntegerField(null=False, primary_key=True)
    city_name = CharField(max_length=200)
    city_lat = DecimalField(max_digits=9, decimal_places=6)
    city_long = DecimalField(max_digits=9, decimal_places=6)
    city_country = CharField(max_length=3)

    def __str__(self):
        return "(Name : %s, Country : %s, Lat : %s, Long : %s)" %(self.city_name, self.city_country, self.city_lat, self.city_long)

class UserProfile(Model):
    user = OneToOneField(
        User,
        on_delete=CASCADE,
        primary_key=True,
        related_name="+"
    )
    current_location = models.ForeignKey(
        Location,
        on_delete=PROTECT,
        related_name="+"
    )

    def __str__(self):
        return "(Firstname : %s, Lastname : %s, Current location : %s)" %(self.user.first_name, self.user.last_name, self.current_location)


class TrackedLocation(Model):
    user = models.ForeignKey(
        UserProfile,
        on_delete=CASCADE,
        related_name="tracked_locations"
    )
    location = models.ForeignKey(
        Location,
        on_delete=PROTECT,
        related_name="+"
    )