from rest_framework import routers
from django.conf.urls import url
from weatherapp.views import TrackedLocationsViewSet, UserProfileViewSet, CurrentWeatherView,\
    WeatherForecastView, LoginView, IndexView, LogoutView


router = routers.SimpleRouter()
router.register(r'locations/tracked', TrackedLocationsViewSet, base_name='trackedlocations')
router.register(r'user/profile', UserProfileViewSet, base_name='userprofile')
urlpatterns = router.urls

urlpatterns.append(url(r'^weather/current/cities/$', CurrentWeatherView.as_view()))
urlpatterns.append(url(r'^weather/forecast/city/(?P<city_id>[0-9]*)$', WeatherForecastView.as_view()))
urlpatterns.append(url(r'^login/$', LoginView.as_view(), name='login'))
urlpatterns.append(url(r'^logout/$', LogoutView.as_view()))
urlpatterns.append(url(r'^$', IndexView.as_view(), name='index'))