# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-06-28 05:50
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('weatherapp', '0002_auto_20170628_0029'),
    ]

    operations = [
        migrations.RenameField(
            model_name='trackedlocation',
            old_name='user_profile',
            new_name='user',
        ),
    ]
