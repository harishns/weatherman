import base64

from django.contrib.auth.models import AnonymousUser
from rest_framework import viewsets
from rest_framework import permissions
from django.views import View
from django.http import HttpResponse, JsonResponse
from django.shortcuts import redirect, render
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic.base import TemplateView

from weatherapp.models import TrackedLocation, UserProfile, Location
from weatherapp.serializers import TrackedLocationSerializer,UserProfileSerializer
from weatherapp.permissions import IsOwner
from weatherapp.weather_tracker import get_weather_tracker_instance


def get_authenticated_user(request):
    user = request.user
    if request.META.get('HTTP_AUTHORIZATION', None):
        authmeth, auth = request.META['HTTP_AUTHORIZATION'].split(' ', 1)
        if authmeth.lower() == 'basic':
            auth = base64.b64decode(auth.strip())
            username, password = auth.split(b':', 1)
            user = authenticate(username=username, password=password)
    return user

class TrackedLocationsViewSet(viewsets.ModelViewSet):
    serializer_class = TrackedLocationSerializer
    permission_classes = (permissions.IsAuthenticated,
                          IsOwner,)

    def get_serializer(self, *args, **kwargs):
        if "data" in kwargs:
            data = kwargs["data"]

            if isinstance(data, list):
                kwargs["many"] = True

        return super(TrackedLocationsViewSet, self).get_serializer(*args, **kwargs)

    def get_queryset(self):
        tracked_locations = TrackedLocation.objects.filter(user__user__id=self.request.user.id)
        return tracked_locations

    def perform_create(self, serializer):
        data = self.request.data
        user = self.request.user
        serializer.save(user=UserProfile.objects.get(user=user))


class UserProfileViewSet(viewsets.ModelViewSet):
    serializer_class = UserProfileSerializer
    permission_classes = (permissions.IsAuthenticated,
                              IsOwner,)

    def get_queryset(self):
        return UserProfile.objects.filter(user__id=self.request.user.id)

    def perform_update(self, serializer):
        user = self.request.user
        serializer.save(user=user)

class CurrentWeatherView(View):

    def get(self, request):
        user = request.user
        if isinstance(user, AnonymousUser):
            user = get_authenticated_user(request)
        tracked_city_map = {}
        tracked_locations = TrackedLocation.objects.filter(user__user=user)
        if tracked_locations:
            for tl in tracked_locations:
                tracked_city_map[tl.location.city_id] = tl.id
            weather_house = get_weather_tracker_instance()
            weather_data = weather_house.get_current_weather_by_city_id(tracked_city_map=tracked_city_map, many=True)
            return JsonResponse({"data": weather_data})
        else:
            return JsonResponse({"data": False})


class WeatherForecastView (View):

    def get(self, request, city_id):
        if isinstance(request.user, AnonymousUser):
            user = get_authenticated_user(request)

        weather_house = get_weather_tracker_instance()
        weather_data = weather_house.get_weather_forecast_by_city_id(int(city_id))
        return JsonResponse({"forecast_data": weather_data})


class LoginView(TemplateView):
    template_name = "login.html"

    def post(self,request,**kwargs):
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username,
                                 password=password)
        if user is not None and user.is_active:
            login(request, user)
            return redirect('index')

class LogoutView(TemplateView):

    def get(self,request,**kwargs):
        logout(request)
        return redirect("login")


class IndexView(TemplateView):
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        tracked_locations = [tl.location.city_id for tl in TrackedLocation.objects.filter(user__user=self.request.user)]
        context['tracked_locations'] = tracked_locations
        up = UserProfile.objects.get(user=self.request.user)
        context['user_profile'] = up.user_id
        context['current_location'] = up.current_location.city_id
        return context

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(IndexView, self).dispatch(*args, **kwargs)
