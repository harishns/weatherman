from rest_framework import serializers
from weatherapp.models import TrackedLocation,UserProfile


class TrackedLocationListSerializer(serializers.ListSerializer):
    """def create(self, validated_data):
        print("+++++++++++++++++++")
        print(validated_data)
        tracked_locations = [TrackedLocation(**item) for item in validated_data]
        return TrackedLocation.objects.bulk_create(tracked_locations)"""


class TrackedLocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = TrackedLocation
        list_serializer_class = TrackedLocationListSerializer
        fields = ('id', 'location')

class UserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = ('current_location',)