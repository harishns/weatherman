from django.contrib.auth.models import User
from rest_framework import permissions

from weatherapp.models import UserProfile

class IsOwner(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        if isinstance(obj.user, User):
            return request.user == obj.user
        elif isinstance(obj.user, UserProfile):
            return request.user == obj.user.user