import pyowm
from django.conf import settings


def get_select_weather_fields(weather_dict):
    weather_data = weather_dict.get('weather')
    city_weather_data = {"city_id":weather_dict.get('city_id'),
                         "city_name":weather_dict.get('city_name'),
                         "city_country":weather_dict.get('city_country'),
                         "tl_id":weather_dict.get('tl_id')
                         }
    weather_data_short = {}
    weather_data_short['reference_time'] = weather_data.get_reference_time(timeformat='date')
    weather_data_short['humidity'] = weather_data.get_humidity()
    weather_data_short['temperature'] = weather_data.get_temperature(unit='fahrenheit')
    weather_data_short['pressure'] = weather_data.get_pressure()
    weather_data_short['status'] = weather_data.get_status()
    weather_data_short['clouds'] = weather_data.get_clouds()
    weather_data_short['wind'] = weather_data.get_wind()
    weather_data_short['snow'] = weather_data.get_snow()
    city_weather_data['weather'] = weather_data_short
    return city_weather_data


def weather_to_dict(weather_data):
    if isinstance(weather_data, dict):
        return get_select_weather_fields(weather_data)
    elif isinstance(weather_data, list):
        return list(map(lambda x: get_select_weather_fields(x), weather_data))


def get_weather_tracker_instance():
        return WeatherHouse()

class WeatherHouse():
    weather_man = pyowm.OWM(settings.OWM_APIKEY)

    def get_current_weather_by_city_id(self, tracked_city_map, many=False):
        if many:
            city_ids = list(tracked_city_map.keys())
            observations = self.weather_man.weather_at_ids(city_ids)
            weather_list = list(map(lambda o : {"city_id" : o.get_location().get_ID(),
                                                "city_country": o.get_location().get_country(),
                                                "city_name": o.get_location().get_name(),
                                                "tl_id": tracked_city_map[int(o.get_location().get_ID())],
                                                "weather" : o.get_weather()}, observations))
            return weather_to_dict(weather_list)

        city_id = list(tracked_city_map.keys())[0]
        observation = self.weather_man.weather_at_id(city_id)
        weather = {"city_id" : observation.get_location().get_ID(),
                   "city_country": observation.get_location().get_country(),
                   "city_name": observation.get_location().get_name(),
                   "tl_id": tracked_city_map[city_id],
                   "weather" : observation.get_weather()}
        return weather_to_dict(weather)

    def get_weather_forecast_by_city_id(self, city_id):
        forecaster = self.weather_man.daily_forecast_at_id(city_id,5) # hardcoded number of days in forecast to 5
        forecast = forecaster.get_forecast()
        weather_list = list(map(lambda w : {"city_id" : city_id, "weather" : w}, forecast.get_weathers()))
        return weather_to_dict(weather_list)





