 $(document).ready(function() {
    function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
	}
	var csrftoken = getCookie('csrftoken');

	$.ajaxSetup({
	    beforeSend: function(xhr, settings) {
	        if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
	            // Only send the token to relative URLs i.e. locally.
	            xhr.setRequestHeader("X-CSRFToken", csrftoken);
	        }
	    }
	});

	function show_weather_forecast(res, city_name, city_country){
	    res['city_name'] = city_name;
	    res['city_country'] = city_country;
        var weather_forecast_tmpl = _.template($("#city-weather-detail-tmpl").html());
        $("#cityWeatherDetailModal").find(".modal-title").html("<strong>Forecast for " + city_name+", " + city_country + "</strong>")
        $("#cityWeatherDetailModal").find(".modal-body").html(weather_forecast_tmpl(res));
        $("#cityWeatherDetailModal").modal('show');
     }

	function bindEvents() {
        $('#weather-data-container').on('click', '.city-weather-summary', function (ev) {
            if($(ev.target).parent().hasClass('un-track-location')){
                var tl_id = $(ev.target).parent().data('locid');
                var city_id = $(ev.target).parent().data('cityid');
                unTrackLocation(tl_id,city_id);
                return;
            }
            var city_id = $(ev.target).data('cityid');
            var city_name = $(ev.target).data('cityname');
            var city_country= $(ev.target).data('citycountry');
            $.ajax({
                "type": "GET",
                "url": "/weatherapp/weather/forecast/city/"+city_id,
                'success': function(res){
                    show_weather_forecast(res,city_name, city_country);
                },
            });
        });
    }

	function unTrackLocation(tl_id,city_id) {
        var current_city_id = $("#current-city").val();
	    if(city_id==current_city_id){
            alert("The city you are trying to un-track is still your current city ");
        }
        else{
	        $.ajax({
                "type": "DELETE",
                "url": "/weatherapp/locations/tracked/"+tl_id,
                'success': function(res){
                    show_current_weather();
                },
                'complete': function () {
                    var selector = "#cities-list-multiselect option[value=" + city_id + "]" ;
                    $(selector).prop('disabled', false);
                    var tracked_cities = [];
                    $(".city-weather-summary").each(function () {
                        tracked_cities.push($(this).data('cityid'));
                    });

                    $("#cities-list-multiselect").multiselect('deselectAll', true);
                    $("#cities-list-multiselect").multiselect('refresh');
                }
            });
        }
    }

	function show_current_weather() {
        $.ajax({
			"type": "GET",
	        "url": "/weatherapp/weather/current/cities/",
	        'success': function(res){
			    var weather_summary_tmpl = _.template($("#city-weather-summary-tmpl").html());
			    $("#cities-list-multiselect").multiselect('refresh');
			    $("#weather-data-container").html(weather_summary_tmpl(res));
			    bindEvents();
	        },
		});
    }

    function track_cities(locations) {
        if(locations.length > 1){
            var data = [];
            for(var i=0;i<locations.length;i++){
                data.push({"location":locations[i]})
            }
        }
        else if(locations.length == 1){
            var data = {"location":locations[0]};
        }

		$.ajax({
			"type": "POST",
	        "url": "/weatherapp/locations/tracked/",
	        "data": JSON.stringify(data),
            "contentType":"application/json",
	        'success': function(res){
			    show_current_weather();
	        },
		});
    }
    tracked_cities = $("#cities-list-multiselect option:disabled");
	if(tracked_cities.length>0){
	    show_current_weather();
    }

    $('#current-city').multiselect({
            on: {
                change: function(option, checked) {
                    var values = [];
                    $('#current-city').each(function() {
                        if ($(this).val() !== option.val()) {
                            values.push($(this).val());
                        }
                    });

                    $('#current-city').multiselect('deselect', values);
                }
            }
    });

	$('#set-current-city').on('click', function (ev) {
	    var location = $("#current-city").val();
	    var userprofile = $("#current-city").data('up');
	    var data = {"current_location":location};
	    $.ajax({
			"type": "PUT",
	        "url": "/weatherapp/user/profile/"+userprofile + "/",
	        "data": JSON.stringify(data),
            "contentType":"application/json",
	        'success': function(res){
			    track_cities([location]);
	        },
		});
    });

    $('#cities-list-multiselect').multiselect();

    $('#track-cities').on('click', function (ev) {
        var locations = $("#cities-list-multiselect").val();
        track_cities(locations);
    });
 });